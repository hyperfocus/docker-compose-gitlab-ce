#!/bin/bash

#GITLAB_FILEMAX=1000000
#[[ $(cat /proc/sys/fs/file-max) -lt ${GITLAB_FILEMAX} ]] && echo $GITLAB_FILEMAX > /proc/sys/fs/file-max

HOSTNAME=gitlab.local
GITLAB_URL=http://192.168.1.235/
VERSION=8.11.9-ce.0

docker --debug run --detach --name gitlab \
 --hostname ${HOSTNAME} \
 --sysctl net.core.somaxconn=1024 \
 --ulimit sigpending=62793 \
 --ulimit nproc=131072 \
 --ulimit nofile=60000 \
 --ulimit core=0 \
 --publish 4443:443 --publish 20080:80 --publish 2222:22 \
 --restart always \
 --env GITLAB_OMNIBUS_CONFIG="external_url '${GITLAB_URL}'; gitlab_rails['lfs_enabled'] = true; mattermost_external_url '${GITLAB_URL}';" \
 --volume /srv/gitlab/config:/etc/gitlab \
 --volume /srv/gitlab/logs:/var/log/gitlab \
 --volume /srv/gitlab/data:/var/opt/gitlab \
 --volume /etc/localtime:/etc/localtime \
 gitlab/gitlab-ce:${VERSION}
