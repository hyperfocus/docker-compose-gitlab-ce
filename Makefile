gitlab-ce: gitlab-ce-compose

gitlab-ce-compose:
	cd compose && docker-compose up -d

gitlab-ce-run:
	cd run && bash gitlab-ce-run.sh
